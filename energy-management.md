## Question 1:- What are the activities you do that make you relax - Calm quadrant?

- Play games.

## Question 2:- When do you find getting into the Stress quadrant?

- When work pressure is more.

## Question 3:- How do you understand if you are in the Excitement quadrant?

- When my code satisfies me or when I am happy.

## Question 4:- Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

1. Sleep is a superpower that enhances learning and memory.
1. Lack of sleep impairs cognitive functions and poses health risks.
1. Adequate sleep is vital for overall well-being, immune function, and heart health.
1. Sleep plays a role in preventing cognitive decline and Alzheimer's disease.
1. Short sleep duration is linked to gene activity distortions and health problems, including cancer.
1. Maintaining regular sleep patterns and a cool bedroom temperature improves sleep quality.
1. Matt Walker emphasizes the importance of prioritizing sleep and breaking the association between bed and wakefulness.

## Question 5:- What are some ideas that you can implement to sleep better?

1. Establish a consistent sleep schedule by going to bed and waking up at the same time every day.
1. Create a relaxing bedtime routine to signal to your body that it's time to wind down.
1. Keep your sleep environment comfortable, cool, and dark.
1. Limit exposure to screens before bedtime as the blue light can interfere with melatonin production.
1. Be mindful of your diet, avoiding large meals, caffeine, and alcohol close to bedtime.
1. Engage in regular physical activity, but avoid vigorous exercise close to bedtime.
1. Manage stress through relaxation techniques such as deep breathing, meditation, or yoga.

## Question 6:- Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

1. Wendy Suzuki shares her personal journey discovering the positive impact of exercise on the brain.
1. Regular physical activity improved Suzuki's mood, energy levels, and enhanced her hippocampus for long-term memory.
1. Exercise, even in small amounts, has long-lasting benefits, including improved mood, attention, and memory, and protection against neurodegenerative diseases.
1. Suzuki recommends a minimum of 30 minutes of daily exercise, with an emphasis on including aerobic activities.
1. The audience actively participates in a one-minute exercise session, reinforcing Suzuki's message to prioritize regular exercise for brain protection.

## Question 7:- What are some steps you can take to exercise more?

1. **Set Realistic Goals:** Define achievable and specific exercise goals based on your fitness level and schedule.

1. **Find Enjoyable Activities:** Choose exercises or activities you enjoy to make the experience more enjoyable and sustainable.

1. **Create a Routine:** Establish a consistent workout routine, incorporating exercise into your daily or weekly schedule.

1. **Start Small:** Begin with short sessions and gradually increase the duration and intensity as your fitness improves.

1. **Mix it Up:** Keep things interesting by trying different types of exercises to work various muscle groups and prevent boredom.

1. **Involve Others:** Exercise with friends, join group classes, or find a workout buddy to make it a social and motivating activity.

1. **Use Technology:** Utilize fitness apps, trackers, or online workouts to monitor progress and stay motivated.
