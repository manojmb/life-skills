## Question 1:- Which point(s) were new to you?

- Get to know your teammates and bonding.
- Getting frequent feedbacks.
## Question 2:- Which area do you think you need to improve on? What are your ideas to make progress in that area?

**Expanding Domain-Specific Knowledge:**  
Expanding the knowledge base in specific domains related to software development, ensuring it's up-to-date with the latest tools, methodologies, and industry standards.