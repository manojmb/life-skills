## Question 1:- Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.  
In her TED talk, Dr. Angela Duckworth explores the concept of grit, defining it as the combination of passion and perseverance essential for success. 


## Question 2:- Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

The growth mindset, championed by Carol Dweck, emphasizes that skills and intelligence can be cultivated rather than being innate. 


## Question 3:- What is the Internal Locus of Control? What is the key point in the video?  
The internal locus of control is a psychological concept that refers to an individual's belief that they have control over their own life and the events that happen to them.  
The key point in the video is likely centered around the advice on adopting an internal locus of control and staying motivated.

## Question 4:- What are the key points mentioned by speaker to build growth mindset (explanation not needed).

1. Believe in your ability to figure things out.  
1. Question assumptions and be open to learning and expanding knowledge and skills.**  
1. Develop a curriculum to make your dreams possible.  
1. Learn from failures and setbacks, viewing them as opportunities for growth.
1. Honor the struggle and adopt a resilient mindset for long-term growth.
1. Follow a 10-step process for achieving goals faster and succeeding in life.
1. Promote the importance of having a growth mindset for personal and professional growth.
1. Encourage living fully, loving openly, and striving to make a positive difference each day.
1. Offer a free guide on developing a growth mindset.
1. Access the guide by providing your name and email address.  

## Question 5:- What are your ideas to take action and build Growth Mindset?

1. **Embrace Challenges:**
   - Seek out challenges that push your boundaries.
   - View challenges as opportunities to learn and grow.

1. **Learn from Criticism:**
   - See feedback, even if it's critical, as valuable input for improvement.
   - Use criticism to identify areas for growth and development.

1. **Effort is the Path to Mastery:**
   - Understand that effort is a key factor in learning and mastery.
   - Embrace the idea that continuous effort leads to improvement.

2. **Learn from Setbacks:**
   - See setbacks as learning opportunities rather than failures.
   - Analyze what went wrong, adjust, and apply the lessons learned.
  
3. **Seek Feedback and Learn Continuously:**
   - Actively seek feedback on your performance.
   - Continuously seek opportunities for learning and improvement.