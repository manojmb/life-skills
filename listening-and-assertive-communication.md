## Question 1:- What are the steps/strategies to do Active Listening?  
1. Focus on the Speaker and Topic
1. Avoid Interruptions
2. Use Door Openers such as gestures
3. Take Notes
4. Paraphrasing
5. Emphasize Mutual Understanding

## Question 2:- According to Fisher's model, what are the key points of Reflective Listening?
1. **Paraphrasing:**  
        Reflective listening involves paraphrasing or restating the speaker's words in your own language.
        This demonstrates that you are actively engaged in the conversation and trying to understand the speaker's perspective.

2. **Empathetic Responses:**  
        Show empathy by acknowledging and validating the speaker's feelings and emotions.
        Use reflective statements to convey that you understand the emotional content of their message.

3. **Avoiding Judgment:**  
        Suspend judgment and refrain from inserting your opinions or evaluations into the conversation.
        Reflective listening focuses on creating a non-judgmental space for the speaker to express themselves.

4. **Clarification:**  
        Seek clarification when necessary by asking open-ended questions or requesting more information.
        This helps ensure that your understanding aligns with the speaker's intended message.

5. **Non-Verbal Cues:**  
        Use non-verbal cues, such as nodding and maintaining eye contact, to convey attentiveness and interest.
        Non-verbal communication plays a crucial role in reflecting genuine engagement.

6. **Summarization:**  
        Periodically summarize the key points of what the speaker has said to reinforce understanding.
        Summarization helps in consolidating information and confirming mutual understanding.

## Question3:- What are the obstacles in your listening process?

1. **Distractions:**  
 External distractions, such as noise or interruptions, and internal distractions, such as personal thoughts or concerns, can impede the ability to focus on the speaker.

1. **Assumption of Similarity:**  
Assuming that you already know what the speaker is going to say or that their perspective aligns with your own.
1. **Overemphasis on Details:**  
Focusing excessively on specific details or getting caught up in minor points.


## Question 4:- What can you do to improve your listening?
1. **Be Present:**  
Focus on the present moment and the speaker. Minimize distractions, put away electronic devices, and give your full attention to the conversation.

1. **Suspend Judgment:**  
Avoid making premature judgments or forming opinions before fully understanding the speaker's perspective. Keep an open mind and be receptive to different viewpoints.

1. **Practice Mindfulness:**  
Cultivate mindfulness to stay present and focused during conversations. Mindful listening involves being aware of your thoughts and emotions without letting them distract you from the speaker.

## Question 5:- When do you switch to Passive communication style in your day to day life?
1. **Cultural or Social Norms:**  
    Cultural or societal expectations can influence communication styles. In some cultures, there may be an emphasis on avoiding direct confrontation, leading individuals to lean towards passive communication.

1. **Maintaining Relationships:**  
    People might choose passive communication to maintain relationships and avoid rocking the boat. They may prioritize keeping the peace over expressing their needs or opinions.

1. **Personal Fatigue or Stress:**  
    When individuals are tired, stressed, or emotionally drained, they may resort to passive communication as a way to conserve energy and avoid additional emotional strain.


## Question 6:- When do you switch into Aggressive communication styles in your day to day life?

1. **High Stress or Frustration:**

    When individuals are under significant stress or frustration, they may resort to aggressive communication as a way to release pent-up emotions. This can result in lashing out at others.

1. **Feeling Threatened:**

    Perceived threats to one's self-esteem, status, or well-being can trigger aggressive communication. This may involve attacking or belittling others as a defense mechanism.

## Question 7:- When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

**Avoiding Confrontation:**  
Individuals might use passive-aggressive communication to avoid direct confrontation, expressing their dissatisfaction indirectly rather than addressing issues head-on.


## Question 8:- How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

1. **Use "I" Statements:**  
    Express your thoughts and feelings using "I" statements to take ownership of your emotions. For example, say "I feel" or "I think" to convey your perspective without sounding accusatory.

1. **Be Clear and Specific:**  
    Clearly articulate your thoughts, needs, and expectations. Avoid vague or ambiguous language, and be specific about what you want or need from the other person.

1. **Set Boundaries:**  
    Clearly define your boundaries and communicate them assertively. Let others know what behavior is acceptable and unacceptable to you.

1. **Be Respectful of Others:**  
    While expressing your own needs, be respectful of the needs and opinions of others. Acknowledge their perspectives and avoid dismissing or belittling their views.