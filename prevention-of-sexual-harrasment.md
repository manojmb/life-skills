## 1. What kinds of behavior cause sexual harassment?

- Repeatedly asking someone out on a date, particularly if the individual is not interested.
- Utilizing sexual jokes and humor within the workplace.
- Making unwelcome sexual advances or requests.
- Engaging in visual, verbal, or physical conduct of a sexual nature that is unwelcome.
- Failing to respect signals or comprehend the impact of one's behavior on others.
- Threatening an employee with negative consequences or promising rewards in exchange for sexual favors (harassment).
- Establishing a hostile work environment through pervasive unwelcome sexual behavior.

## 2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

- **Follow the organization's grievance procedure:** Report the incident to supervisors, HR, or another trustworthy person, adhering to the established process.
- **Be aware of and acknowledge others' perspectives:** Recognize that what may seem harmless to one person could be perceived as harassment by another.
- **Speak up against harassment:** If you experience or witness sexual harassment, consider speaking out against it and reporting the incident to the appropriate channels.
- **Emphasize awareness, perception, and prevention:** Acknowledge the importance of creating awareness about sexual harassment, understanding different perspectives, and implementing preventive measures in the workplace.
- **Maintain professionalism:** Act ethically, treat everyone equally based on performance, and avoid engaging in behaviors that could contribute to a hostile work environment.

These actions aim to address and prevent sexual harassment in the workplace, fostering a positive and respectful working environment for all employees.
